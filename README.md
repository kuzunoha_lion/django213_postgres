# 参考

以下のサイトを参考にしていまして、もしかしたらそちらの方がわかりやすいかもしれないです。

https://docs.docker.com/compose/django/
https://aoishi.hateblo.jp/entry/2017/11/05/153341

# クイックスタート

`docker-compose.yml`のあるディレクトリで

```
docker-compose build
```
とコマンドを打ってください。これはDockerImageのダウンロードと作成を行います。そのため、一度このコマンドを打てば、次回からはこの工程は必要ありません。ただし、Dockerfileを更新した場合は、改めてこのコマンドを打つ必要があります。

```
docker-compose run web django-admin.py startproject myproject .
```

このコマンドを打つとプロジェクトを作成できます。なお、二回目以降は必要ありません。また、`myproject`は好きなプロジェクト名を入れていただいても構いません。

以下のようなログが出てきます。

```
Creating network "django213_postgres_default" with the default driver
Creating postgres_ctr ... done
```

そうしますと下記のファイルとフォルダ出来ます。

```
code/myproject/
db.sqlite3
manage.py
```

特に`/myproject`はコンテナ内の`root`の権限になっているので、`sudo chown -R $USER:$USER app/code/myproject/`を行うと良いでしょう。

そうしましたら下記コマンドを打ってください。

```
docker-compose up -d
```

これは`/code/manage.py runserver 0.0.0.0:8081`を行っています。そうしたら以下のログが出てきます。

```
postgres_ctr is up-to-date
Creating django_ctr ... done
```

そうしましたら以下のURLを開いてください。

```
[Docker]
127.0.0.1:8081
[DockerToolbox]
192.168.99.100:8081
```

Djangoのデバッグ画面が出ているはずです。（３秒ほど待ちはありますけど。）

Dockerを閉じる、作業をやめる、といった場合は以下のコマンドを打ってください。

```
docker-compose down
```

PostgreSQLコンテナとつなぐ場合は`setting.py`のうち、`DATABASES = {....`を以下に変更してください。

なお、ここまで順当にやっているなら心配ない話ですが、`setting.py`は` django-admin.py startproject myproject .`を打つまで出現しません。

```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'post',
        'PASSWORD': '881144post',
        'USER': 'postman',
        'HOST': 'db',
        'PORT': 5432,
    }
}
```

DBへのマイグレートは以下のコマンドの通り

```
docker-compose exec web python manage.py migrate
```

その他、DockerのPythonコンテナへコマンドを実行したい場合は以下の通りになります。

```
docker-compose exec web [シェルのコマンド]
```

例としては`docker-compose exec web python manage.py makemigrations polls`とかですかね？ここから先はDjangoの勉強かと思慮します…

# 細々したところについて
## コンテナのログについて

以下のコマンドでPython用コンテナのログを出力できます。

```
docker-compose logs web
```

## DockerのPython環境のモジュールを変更したい場合

ディレクトリ内に`requirements.txt`というものがあります。これを更新しImageを更新すると反映されます。

`requirements.txt`は以下のようになっています。
追加したいor変更したい場合は同じように`[Module名]==[Module Version]`で書いてあげてください。

```
[Module名]==[Module Version]
Django==2.1.3
psycopg2==2.7.6
psycopg2-binary==2.7.6
uwsgi==2.0.17
```

DockerImageの更新コマンドは以下の通り

```
docker-compose build
```

## Imageの更新とゴミ

DockerImageを新しいものにするたびにBuildするわけですが、そのたびにゴミが出ることになります。

```
docker images
```

を打つと今ローカルにもっているImageが出力されます。

```
REPOSITORY                        TAG                 IMAGE ID            CREATED             SIZE
django                            2.1.3               3273749c08c5        2 days ago          980MB
<none>                            <none>              e8b40fedc683        3 days ago          980MB
postgres                          10.6                6785cf0e01c9        4 days ago          228MB
gcr.io/*******************/test   0.111               8128d7aa58c7        10 days ago         971MB
test                              test                434c20685f70        10 days ago         971MB
```

まぁこいつです。

```
REPOSITORY                        TAG                 IMAGE ID            CREATED             SIZE
<none>                            <none>              e8b40fedc683        2 days ago          980MB
```

`REPOSITORY <none> TAG <none>`というのは名前をつけないでイメージを作ったか、同じ名前、同じタグのイメージを作った場合に、古いものがこのようになります。大体が邪魔なものだと思うので削除しましよう

```
docker rmi <IMAGE ID>
```

今回の場合は`docker rmi e8b40fedc683`です。

## Djangoの日本語化

`setting.py`の内、以下を設定しましょう。

なお、`setting.py`は` django-admin.py startproject myproject .`を打つまで出現しません。

`LANGUAGE_CODE = 'en-us'`を`LANGUAGE_CODE = 'ja'`へ

`TIME_ZONE = 'UTC'` を `TIME_ZONE = 'Asia/Tokyo'`へ

それぞれ設定しましょう。

```
# LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'ja'

# TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Tokyo'
```

# なんかあったら

いちおーツイッターやってるんでどぞ…
https://twitter.com/AExtab
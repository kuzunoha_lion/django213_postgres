# https://docs.docker.com/compose/django/#define-the-project-components
FROM python:3.6.6
# setting
ENV PYTHONUNBUFFERED 1
# pip インストールリストのコピー
COPY requirements.txt /tmp/requirements.txt
# インストールリストを用いたpipによるmoduleのダウンロード
RUN pip install -r /tmp/requirements.txt
# share用dir作成 + chdir
RUN mkdir /code/
WORKDIR /code/